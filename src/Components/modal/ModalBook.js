import React, { useState } from 'react'
import Modal from '@material-ui/core/Modal'
import Box from '@material-ui/core/Box'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import styled from 'styled-components'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

const StyledButton = styled(Button)`
  border-radius: 25%;
`
const ModalStyled = styled(Modal)`
  display: flex;
  align-items: center;
  justify-content: center;
`
const StyledInput = styled(TextField)`
  width: 100%;
`
const StyledBox = styled(Box)`
  border-radius: 8px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 400px;
  height: 500px;
  background-color: #fff;
  padding: 32px;
`

const Container = styled.div`
  flex-direction: row;
  display: flex;
  justify-content: space-between;
  width: 50%;
  @media (max-width: 420px) {
    width: 100%;
    justify-content: space-around;
  }
`

const ModalBook = props => {
  const [bookData, setBookData] = useState({})

  return (
    <div>
      <ModalStyled
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={props.open}
        onClose={props.onClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={props.open}>
          <StyledBox>
            <StyledInput
              disabled
              type='number'
              defaultValue={props.data.id}
              label="Id"
            />
            <StyledInput
              onChange={e => setBookData({...bookData, name: e.target.value})}
              defaultValue={props.data.name}
              label="Name"
            />
            <StyledInput
              onChange={e => setBookData({...bookData, genre: e.target.value})}
              defaultValue={props.data.genre}
              label="Genre"
            />
            <StyledInput
              onChange={e => setBookData({...bookData, author: e.target.value})}
              defaultValue={props.data.author}
              label="Author"
            />
            <StyledInput
              onChange={e => setBookData({...bookData, year: e.target.value})}
              type='number'
              defaultValue={props.data.year}
              label="Year"
            />
            <Container>
              <StyledButton
                onClick={() => props.onClickCancel()}
                variant="contained"
                color="secondary"
              >Cancel</StyledButton>
              <StyledButton
                onClick={() => {
                  props.onClickSaveChages({...props.data, ...bookData})
                  setBookData({})
                }}
                variant="contained"
                color="primary"
              >Save</StyledButton>
            </Container>
          </StyledBox>
        </Fade>
      </ModalStyled>
    </div>
  )
}

export default ModalBook
