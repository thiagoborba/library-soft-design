import React  from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles({
  card: {
    minWidth: 275,
    margin: 32,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
})

const BookCard = props =>  {
  const classes = useStyles()
  const { book } = props

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          { book.name }
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          { book.author }
        </Typography>
        <Typography variant="body2" component="p">
          { book.genre }
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          variant='contained'
          onClick={props.onClickUpdate}
          disabled={book.rented}
          size="small"
        >
          Update
        </Button>
        <Button
          variant='contained'
          disabled={book.rented}
          size="small"
          onClick={props.onClickDelete}
        >
          Delete
        </Button>
        <Button
          variant={book.rented ? 'outlined' : 'contained'}
          size="small"
          onClick={props.onClickRent}
        >
          Rent
        </Button>
      </CardActions>
    </Card>
  )
}

export default BookCard