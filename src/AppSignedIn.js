import React from 'react'
import { withContext } from './Library/AppContext'
import { Route } from 'react-router-dom'
import BooksPage from './Components/Pages/BooksPage/BooksPage'
import Structure from './Components/structure/Structure'

const AppSignedIn = () => {
  return (
    <Structure>
      <Route path='/' exact component={BooksPage} />
    </Structure>
  )
}

export default withContext(AppSignedIn)
