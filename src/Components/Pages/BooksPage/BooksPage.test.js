import React from 'react'
import BooksPage from './BooksPage'
import Enzyme, { render } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('Component: BooksPage', () => {
  it('should display a table', () => {
    const wrapper = render(<BooksPage/>)
    expect(wrapper.find('Table').length).toBe(1)
  })
})