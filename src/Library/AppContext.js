import React, { useState } from 'react'
import { mockBooks, mockUsers } from './mock'

const { Provider, Consumer } = React.createContext({})

const withContext = (BaseComponent) => {
  return class extends React.Component {
    static displayName = `withContext(${BaseComponent.name})`

    render () {
      return (
        <Consumer>
          {context => {
            return (
              <BaseComponent {...this.props} library={context} />
            )
          }}
        </Consumer>
      )
    }
  }
}

const AppContextProvider = props => {
  const [ books, setBooks ] = useState(mockBooks)
  const [ users ] = useState(mockUsers)
  const [ searchBooks, setSearchBooks ] = useState([])
  const [ user, setUser ] = useState(false)

  function getContextFunctions () {
    return {
      login,
      logout,
      addBook,
      updateBook,
      deleteBook,
      searchBook,
    }
  }

  function searchBook (value) {
    if (value !== '') {
      const filtredBooks = books.filter(book => {
        if (book.name.toLowerCase().includes(value.toLowerCase())) {
          return book
        }
        return false
      })
      setSearchBooks(filtredBooks)
    } else {
      setSearchBooks([])
    }
  }

  function addBook (book) {
    const booksState = books
    booksState.push(book)
    setBooks(booksState)
  }

  function updateBook (book) {
    const updatedBooks = books.map(b => {
      if (b.id === book.id) return book
      return b
    })
    setBooks(updatedBooks)
  }

  function deleteBook (book) {
    const filtredBooks = books.filter(b => b.id !== book.id)
    setBooks(filtredBooks)
  }

  function logout () {
    setUser(false)
  }

  function login (email, password) {
    const userExists = users.filter(user => user.email === email && user.password === password)

    if (userExists.length) {
      setUser(userExists[0])
    }
  }

  return (
    <Provider
      value={{books, users, user, searchBooks, ...getContextFunctions()}}
    >
      { props.children }
    </Provider>
  )
}

export {
  withContext,
  AppContextProvider
}