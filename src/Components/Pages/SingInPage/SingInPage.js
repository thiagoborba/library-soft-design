import React, { useState } from 'react'
import { withContext } from '../../../Library/AppContext'
import styled from 'styled-components'
import Page from '../Page'
import Button from '@material-ui/core/Button'
import Input from '@material-ui/core/Input'

const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`

const Form = styled.form`
  box-sizing: border-box;
  position: relative;
  border-radius: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 400px;
  height: 400px;
  padding: 16px;
  background-color: #fff;
  @media (max-width: 420px) {
    width: 100vw;
    height: 100vh;
    border-radius: 0;
    }
`
const Header = styled.div`
  display: flex;
  border-radius: 8px 8px 0 0;
  align-items: center;
  box-sizing: border-box;
  padding: 8px;
  width: 100%;
  height: 60px;
  background-color: #3f51b5;
  position: absolute;
  top: 0;
  text-align: left;
  color: #fff;
` 

const SingInPage = (props) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  function handleFormSubmit (e) {
    e.preventDefault()
    props.library.login(email, password)
  }
  
  return (
    <Page
      backgroundColor={'#DCDCDC'}
    >
      <Container>
        <Form
          onSubmit={handleFormSubmit}
        >
          <Header>
            <h3>Login Form</h3>
          </Header>
          <Input
            style={{ width: '80%', margin: '16px', borderRadius: '4px' }}
            placeholder="Email"
            type='email'
            onChange={event => setEmail(event.target.value)} 
          />
          <Input
            style={{ width: '80%', margin: '16px', borderRadius: '4px' }}
            placeholder="Password"
            type='password' 
            onChange={event => setPassword(event.target.value.toString())} 
          />
          <Button
            variant="contained"
            color="primary"
            type='submit'
          >
            Login
          </Button>
        </Form>
      </Container>
    </Page>
  )
}

export default withContext(SingInPage)
