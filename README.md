# Commands

This project uses makefile, docker and yarn.

## Makefile commands

In the project directory, you can run:

### `make run`

run `yarn start` for up the app in the development mode.

### `make setup`

run the `yarn install` command to install all dependencies on this project.

### `make run-docker`

build the app in a docker container. It`s necessary open a browser and accesing <http://localhost:8000>.
