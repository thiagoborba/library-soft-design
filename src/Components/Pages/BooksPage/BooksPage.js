import React, { useState } from 'react'
import Page from '../Page'
import BookCard from '../../cards/BookCard'
import { Grid, Fab, makeStyles } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import ModalBook from '../../modal/ModalBook'
import { withContext } from '../../../Library/AppContext'
import styled from 'styled-components'
import uuid from 'uuid'

const StyledGrid = styled(Grid)`
  width: 100%;
  overflow: auto;
  margin: 0 0 0 0;
  @media (max-width: 420px) {
    height: 100%;
    }
`
const useStyles = makeStyles(theme => ({
  fab: {
    margin: theme.spacing(1),
    position: 'absolute',
    right: 0,
  },
}))


const BooksPage = ({ library: { books, updateBook, deleteBook, searchBooks, addBook }}) => {
  const [open, setOpen] = useState(false)
  const [selectedBook, setSelectedBook] = useState({})

  const classes = useStyles()

  function handleClickCreate () {
    openModal()
  }

  function handleOnClickRent (book) {
    const bookRented = {
      ...book,
      rented: !book.rented
    }
    updateBook(bookRented)
  }

  function handleClickUpdateCard (book) {
    setSelectedBook(book)
    openModal()
  }

  function openModal () {
    setOpen(true)
  }

  function handleClose () {
    setOpen(false)
  }

  function handleOnClickSaveChages (data) {
    console.log(data)
    if (data.id) {
      handleClose()
      setSelectedBook({})
      updateBook(data)
    } else {
      data.id = uuid()
      data.rented = false
      handleClose()
      setSelectedBook({})
      addBook(data)
    }
  }

  function renderCard (book) {
    return (
      <Grid item key={book.id}>
        <BookCard
          onClickUpdate={() => handleClickUpdateCard(book)}
          onClickRent={() => handleOnClickRent(book)}
          onClickDelete={() => deleteBook(book)}
          book={book}
        />
      </Grid>
    )
  }

  return (
    <Page
      backgroundColor={'#DCDCDC'}
    >
      <Fab color="primary" aria-label="add" className={classes.fab}
        onClick={() => handleClickCreate()}
      >
        <AddIcon />
      </Fab>
      <StyledGrid
        spacing={1}
        container
        alignItems='flex-start'
        justify='flex-start'
      >
        { searchBooks.length ? searchBooks.map(book => renderCard(book)) : books.map(book => renderCard(book)) }
      </StyledGrid>
      <ModalBook
        onClickSaveChages={data => handleOnClickSaveChages(data)}
        onClickCancel={handleClose}
        data={selectedBook}
        open={open}
        onClose={handleClose}
      />
    </Page>
  )
}

export default withContext(BooksPage)
