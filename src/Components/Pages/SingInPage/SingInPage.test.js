import React from 'react'
import SingInPage from './SingInPage'
import Enzyme, { render } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('Component: SingInPage', () => {
  it('should display a form', () => {
    const wrapper = render(<SingInPage />)
    expect(wrapper.find('form').length).toBe(1)
  })

  it('should display two inputs to fill', () => {
    const wrapper = render(<SingInPage />)
    expect(wrapper.find('input').length).toBe(2)
  })

  it('spuld display a button', () => {
    const wrapper = render(<SingInPage />)
    expect(wrapper.find('button').length).toBe(1)
  })
})