import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  box-sizing: border-box;
  width: 100vw;
  height: 100vh;
  background-color: ${ ({ backgroundColor }) => backgroundColor };
  margin: 0;
  padding: 0;
`

const Page = ({ children, backgroundColor }) => {
  return (
    <Container
      backgroundColor={backgroundColor}
    >
      { children }
    </Container>
  )
}


export default Page