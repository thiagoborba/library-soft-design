run:
	yarn start
setup:
	yarn install
run-docker:
	docker build . -t react-docker
	docker run -p 8000:80 react-docker