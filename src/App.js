import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { AppContextProvider } from './Library/AppContext'
import AppAuthenticationControler from './AppAuthenticationControler'

function App() {
  return (
    <BrowserRouter>
      <AppContextProvider>
        <AppAuthenticationControler />
      </AppContextProvider>
    </BrowserRouter>
  )
}

export default App
