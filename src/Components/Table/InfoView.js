import React from 'react'
import styled from 'styled-components'

const Text = styled.p`
  margin: 0;
`

const InfoView = ({ selectedBook }) => {
  return (
    <div
    >
      { selectedBook.id && (
        <div
          style={{ margin: '16px', width: '300px', border: '1px solid #000', padding: '8px' }}
          key={selectedBook.id}
        >
          <h3>{  selectedBook.name }</h3>
          <Text>id: { selectedBook.id }</Text>
          <Text>description: { selectedBook.description }</Text>
          <Text>author: { selectedBook.author }</Text>
          <Text>publishingBy: { selectedBook.publishingBy }</Text>
          <Text>price: { selectedBook.price }</Text>
          <Text>rented: { selectedBook.rented ? 'yes' : 'no' }</Text>
        </div>
      ) }
    </div>
  )
}

export default InfoView