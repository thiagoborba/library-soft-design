import React from 'react'

import SignInPage from './Components/Pages/SingInPage/SingInPage'
import AppSignedIn from './AppSignedIn'

import { withContext } from './Library/AppContext'


const AppAuthenticationControler = props => {
  function renderSignedOut () {
    return (
      <SignInPage />
    )
  }

  function renderSignedIn () {
    return (
      <AppSignedIn />
    )
  }

  function render () {
    if (props.library.user) {
      return renderSignedIn()
    }

    return renderSignedOut()
  }

  return (
    render()
  )
}

export default withContext(AppAuthenticationControler)
