const mockBooks = [
  {
    id: '93145a68-83c5-442e-9732-0790b25b4bc6',
    name: 'MRS. Dalloway',
    genre: 'Romance, Romance psicológico',
    author: 'Virginia Woolf',
    year: 1925,
    rented: false,
  },
  {
    id: '49d11dd2-6666-4761-8db3-29d0cfd26a2b',
    name: 'O Triste fim de policarpo Quaresma',
    genre: 'Ficção',
    author: 'Lima Barreto',
    year: 1915,
    rented: false,
  },
  {
    id: '525f6b06-41f4-4f76-8a10-1d728d95aa66',
    name: 'The Picture of Dorian Gray',
    genre: 'Ficção gótica, Romance filosófico',
    author: 'Oscar Wilde',
    year: 1890,
    rented: false,
  },
  {
    id: 'bb2da20c-9d07-4cca-9834-be6e19b5a686',
    name: '1984',
    genre: 'Ficção Científica Social, Ficção Política',
    author: 'George Orwell',
    year: 1949,
    rented: false,
  },
  {
    id: '6bf105fe-83a7-40ad-9a4e-164d168ed8dc',
    name: 'Animal Farm',
    genre: 'Ficção Política',
    author: 'George Orwell',
    year: 1945,
    rented: false,
  },
]

const mockUsers = [
  {
    email: 'thiago@gmail.com',
    password: '12345678'
  },
  {
    email: 'larissa@gmail.com',
    password: '12345678'
  },
  {
    email: 'juliana@gmail.com',
    password: '12345678'
  },
  {
    email: 'will@gmail.com',
    password: '12345678'
  },
]

export {
  mockBooks,
  mockUsers
}