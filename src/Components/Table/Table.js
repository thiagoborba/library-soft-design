import React from 'react'
import styled from 'styled-components'

const Container = styled.table`
  border-radius: 4px;
  height: 50%;
  width: 50%;
  margin: 16px;
  border: 1px solid #000;
  
`
const TD = styled.td`
  text-align: center;
  padding: 0px;
`
const TH = styled.th`
  text-align: center;
`

const TR = styled.tr`
  &:hover {
    background-color: #c4c4c4;
  }
`

const Table = ({ data, onClick }) => {
  return (
    <Container>
      <thead>
        <TR>
          <TH>name</TH>
          <TH>price</TH>
          <TH>rented</TH>
        </TR>
      </thead>
      <tbody>
        { data && data.map(book => {
          return (
            <TR
              key={book.id}
              onClick={() => onClick(book)}
            >
              <td>{ book.name }</td>
              <TD>{ book.price }</TD>
              <TD>{ book.rented ? 'yes' : 'no' }</TD>
            </TR>
          )
        }) }
      </tbody>
    </Container>
  )
}

export default Table